/*
 * CircularBuffer.c
 *
 *  Created on: Sep 20, 2018
 *      Author: chungnguyena1@gmail.com
 *  2021-08-16:	chungnt@epi-tech.com.vn
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef _CICULAR_BUFFER_H
#define _CICULAR_BUFFER_H



/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "All_User_Lib.h"
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
typedef struct
{
	uint8_t *buf;  // luu dia chi cua mang cBuff can dung
	uint16_t size;          // kich thuoc mang cbuf
	uint16_t tail;          // phan tu doc
	uint16_t head;          // phan tu viet
	uint16_t count;         // bien dem so phan tu da ghi
	uint8_t flagRevEn;
} cicularBuf_t;

/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void CBUFFER_Init(cicularBuf_t *cBuf, uint8_t *address, uint16_t size);
uint8_t CBUFFER_Putc(cicularBuf_t *cBuf, uint8_t data);
uint8_t CBUFFER_Getc(cicularBuf_t *cBuf, uint8_t *data);
void CBUFFER_Reset(cicularBuf_t *cBuf);

#endif
#ifdef __cplusplus
}
#endif
