/*
 * uartUser.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef UARTUSER_H
#define UARTUSER_H


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "All_User_Lib.h"
/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define DEBUG_EN 1

#define MAX_NUM_DATA 100
typedef struct
{
	uint8_t Data[MAX_NUM_DATA];
	uint8_t Byte;
	uint8_t Index;
	uint8_t State;
} buffer_t;

typedef enum
{
	CELL_INDEX = 0,
	CONFIG_INDEX,
	RS232_INDEX,
	GNSS_INDEX,
	MAX_UART_INDEX
} uartIndex_t;

typedef void (*usartBitCfg)(CKCU_PeripClockConfig_TypeDef*);

typedef struct
{
	usartBitCfg clkFunc;
	/* ------------------------------ */
	HT_GPIO_TypeDef* HT_GPIOx;
	u16 GPIO_PIN;
	/* ------------------------------ */
	u32 AFIO_GPIOP_TX;
	u32 AFIO_PIN_TX;
	u32 AFIO_GPIOP_RX;
	u32 AFIO_PIN_RX;
	/* ------------------------------ */
	HT_USART_TypeDef* USARTx;
	IRQn_Type IRQn;
	/* ------------------------------ */
	CKCU_PeripPrescaler_TypeDef Perip;
} Uart_Use_Typedef;
/***************************  USART0-CELLULAR  ***************************************//**/
#define CELL_TX_GPIOX                    A
#define CELL_TX_GPION                    8
#define CELL_RX_GPIOX                    A
#define CELL_RX_GPION                    10
#define CELL_IPN                         USART0

#define CELL_TX_GPIO_ID                  STRCAT2(GPIO_P,         CELL_TX_GPIOX)
#define CELL_RX_GPIO_ID                  STRCAT2(GPIO_P,         CELL_RX_GPIOX)
#define CELL_TX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      CELL_TX_GPION)
#define CELL_RX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      CELL_RX_GPION)
#define CELL_PORT                        STRCAT2(HT_,            CELL_IPN)
#define CELL_IRQn                        STRCAT2(CELL_IPN, _IRQn) 
//#define USART0_IRQHandler                  
#define CELL_RX_GPIO_CLK                 STRCAT2(P,              CELL_RX_GPIOX)
#define CELL_RX_GPIO_PORT                STRCAT2(HT_GPIO,        CELL_RX_GPIOX)
#define CELL_RX_GPIO_PIN                 STRCAT2(GPIO_PIN_,      CELL_RX_GPION)
#define CELL_CKCU_PCLK                   STRCAT2(CKCU_PCLK_,     CELL_IPN)

/************************* PDMA USART0 ***************************************//**/
#define CELL_TX_CH											 CH1
#define CELL_RX_CH											 CH0

#define CELL_TX_PDMA_CH                  STRCAT2(PDMA_, CELL_TX_CH)
#define CELL_RX_PDMA_CH                  STRCAT2(PDMA_, CELL_RX_CH)
#define CELL_TX_PDMA_IRQ                 STRCAT3(PDMA, CELL_TX_CH, _IRQn)
#define CELL_RX_PDMA_IRQ                 STRCAT3(PDMA, CELL_RX_CH, _IRQn)
#define CELL_PDMA_IRQHandler          	 PDMA_CH0_1_IRQHandler
#define CELL_BUF_AVAILABLE    	         (HT_PDMA->PDMACH0.CTSR >> 16)
#define CELL_RESET_DESADD(add)     			 (HT_PDMA->PDMACH0.DADR = add)
#define CELL_RESET_COUNTER(BlkCnt)		   (HT_PDMA->PDMACH0.TSR = ((BlkCnt << 16) | 1))

/***************************  UART0-DEBUG-CONFIG  ***************************************//**/
#define CONFIG_TX_GPIOX                    B
#define CONFIG_TX_GPION                    7
#define CONFIG_RX_GPIOX                    B
#define CONFIG_RX_GPION                    8
#define CONFIG_IPN                         UART0

#define CONFIG_TX_GPIO_ID                  STRCAT2(GPIO_P,         CONFIG_TX_GPIOX)
#define CONFIG_RX_GPIO_ID                  STRCAT2(GPIO_P,         CONFIG_RX_GPIOX)
#define CONFIG_TX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      CONFIG_TX_GPION)
#define CONFIG_RX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      CONFIG_RX_GPION)
#define CONFIG_PORT                        STRCAT2(HT_,            CONFIG_IPN)
#define CONFIG_IRQn                        STRCAT2(CONFIG_IPN, _IRQn)   
//#define USART1_IRQHandler                  
#define CONFIG_RX_GPIO_CLK                 STRCAT2(P,              CONFIG_RX_GPIOX)
#define CONFIG_RX_GPIO_PORT                STRCAT2(HT_GPIO,        CONFIG_RX_GPIOX)
#define CONFIG_RX_GPIO_PIN                 STRCAT2(GPIO_PIN_,      CONFIG_RX_GPION)
#define CONFIG_CKCU_PCLK                   STRCAT2(CKCU_PCLK_,     CONFIG_IPN)

/***************************   USART1-RS232  ***************************************//**/
#define RS232_TX_GPIOX                    A
#define RS232_TX_GPION                    4
#define RS232_RX_GPIOX                    A
#define RS232_RX_GPION                    5
#define RS232_IPN                         USART1

#define RS232_TX_GPIO_ID                  STRCAT2(GPIO_P,         RS232_TX_GPIOX)
#define RS232_RX_GPIO_ID                  STRCAT2(GPIO_P,         RS232_RX_GPIOX)
#define RS232_TX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      RS232_TX_GPION)
#define RS232_RX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      RS232_RX_GPION)
#define RS232_PORT                        STRCAT2(HT_,            RS232_IPN)
#define RS232_IRQn                        STRCAT2(RS232_IPN, _IRQn)  
//#define UART3_IRQHandler                  
#define RS232_RX_GPIO_CLK                 STRCAT2(P,              RS232_RX_GPIOX)
#define RS232_RX_GPIO_PORT                STRCAT2(HT_GPIO,        RS232_RX_GPIOX)
#define RS232_RX_GPIO_PIN                 STRCAT2(GPIO_PIN_,      RS232_RX_GPION)
#define RS232_CKCU_PCLK                   STRCAT2(CKCU_PCLK_,     RS232_IPN)

/***************************   UART1-GNSS  ***************************************//**/
#define GNSS_TX_GPIOX                    C
#define GNSS_TX_GPION                    12
#define GNSS_RX_GPIOX                    C
#define GNSS_RX_GPION                    13
#define GNSS_IPN                         UART1

#define GNSS_TX_GPIO_ID                  STRCAT2(GPIO_P,         GNSS_TX_GPIOX)
#define GNSS_RX_GPIO_ID                  STRCAT2(GPIO_P,         GNSS_RX_GPIOX)
#define GNSS_TX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      GNSS_TX_GPION)
#define GNSS_RX_AFIO_PIN                 STRCAT2(AFIO_PIN_,      GNSS_RX_GPION)
#define GNSS_PORT                        STRCAT2(HT_,            GNSS_IPN)
#define GNSS_IRQn                        STRCAT2(RS232_IPN, _IRQn)  
//#define UART0_IRQHandler                  
#define GNSS_RX_GPIO_CLK                 STRCAT2(P,              GNSS_RX_GPIOX)
#define GNSS_RX_GPIO_PORT                STRCAT2(HT_GPIO,        GNSS_RX_GPIOX)
#define GNSS_RX_GPIO_PIN                 STRCAT2(GPIO_PIN_,      GNSS_RX_GPION)
#define GNSS_CKCU_PCLK                   STRCAT2(CKCU_PCLK_,     GNSS_IPN)

#if(DEBUG_EN == 1)
#define DEBUG(str, ...) printf(str, ##__VA_ARGS__)
#else
#define DEBUG(str, ...)
#endif
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */





/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
/*************************************************************************************************************
  * @brief  Configure the UART
  * @retval None
  ***********************************************************************************************************/
/*

USART_PARITY_NO
USART_PARITY_EVEN
USART_PARITY_ODD

USART_WORDLENGTH_7B          
USART_WORDLENGTH_8B         
USART_WORDLENGTH_9B     

USART_STOPBITS_1
USART_STOPBITS_2
*/
void UART_Cfg(uartIndex_t uartIndex, u32 baudRate, u16 parity, u16 dataLength, u16 stopBit);

/*************************************************************************************************************
  * @brief  Transmit data
  * @retval None
  ***********************************************************************************************************/
void UART_SendByte(HT_USART_TypeDef* USARTx, uint8_t xByte);
void UART_SendString(HT_USART_TypeDef* USARTx, uint8_t *str);

/*************************************************************************************************************
  * @brief  Process Received data
  * @retval None
  ***********************************************************************************************************/
void UART_Process(void);

#endif
#ifdef __cplusplus
}
#endif
