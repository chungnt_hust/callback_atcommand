/*
 * MQTT_HwLayer.c
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
*/
/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "MQTT_HwLayer.h"


/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */
#define DEFAULT_TIMEOUT_CONNECT 180 // 180s
#define DEFAULT_TIMEOUT_PUBLISH 30	// 30s

#define TIME_GET_SIGNAL 5
#define RETRY			 			0
#define CANCEL		   		6

#define MQTT_START_SERVICE 		"AT+CMQTTSTART\r\n"																// Start MQTT service
#define MQTT_ACQUIRE_CLIENT 	"AT+CMQTTACCQ=%d,\"%s\"\r\n"											// Acquire a client %d: client index, %s: client name
#define MQTT_CONNECT_SERVER 	"AT+CMQTTCONNECT=%d,\"tcp://%s:%d\",%d,%d\r\n"    // Connect to MQTT server %d: client index, %s: server add, %d: Port, %d: keep alive time, %d: clean session
#define MQTT_INPUT_PUB_TOPIC 	"AT+CMQTTTOPIC=%d,%d\r\n"													// Input the topic of publish message %d: client index, %d: topic length
#define MQTT_INPUT_PUB_MES 		"AT+CMQTTPAYLOAD=%d,%d\r\n"												// Input the publish message %d: client index, %d: message length
#define MQTT_PUB_MES 					"AT+CMQTTPUB=%d,%d,60,0\r\n"											// Publish a message to server %d: client index, %d: qos
#define MQTT_INPUT_SUB_TOPIC 	"AT+CMQTTSUBTOPIC=%d,%d,%d\r\n"										// Input the topic of subscribe message %d: client index, %d: topic length, %d: qos
#define MQTT_SUB_MES 					"AT+CMQTTSUB=%d\r\n"															// Subscribe a message to server %d: client index
#define MQTT_DISCONN_SERVER 	"AT+CMQTTDISC=%d,60\r\n"													// Disconnect from server %d: client index
#define MQTT_RELEASE_CLIENT 	"AT+CMQTTREL=%d\r\n"															// Release the client %d: client index
#define MQTT_STOP_SERVICE 		"AT+CMQTTSTOP\r\n"																// Stop MQTT Service

/* ==================================================================== */
/* ======================== global variables ========================== */
/* ==================================================================== */

/* Global variables definitions go here */
Mqtt_Manager_t Mqtt_Manager = {.connect = CANCEL, .needConnect = 0, .connectStatus = 0, .timeoutConnection = 0, .mesIsBeingSent = FALSE};
Mqtt_Params_t Mqtt_Params;

/* ==================================================================== */
/* ========================== private data ============================ */
/* ==================================================================== */

/* Definition of private datatypes go here */
static char bufTopic[50];
/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/* Function prototypes for private (static) functions go here */
static uint8_t checkCRC(const char *buf, const char *rest, uint8_t errPos); // command result code
static void StartService_Callback(AT_ResponseEvent_t event, void *ResponseBuffer);
static void AcquireClient_Callback(AT_ResponseEvent_t event, void *ResponseBuffer);
static void Conn2MqttServer_Callback(AT_ResponseEvent_t event, void *ResponseBuffer);
static void InputTopic2Pub_Callback(AT_ResponseEvent_t event, void *ResponseBuffer);
static void InputMes2Pub_Callback(AT_ResponseEvent_t event, void *ResponseBuffer);
static void Pub_Callback(AT_ResponseEvent_t event, void *ResponseBuffer);
static void InputTopic2Sub_Callback(AT_ResponseEvent_t event, void *ResponseBuffer);
static void Sub_Callback(AT_ResponseEvent_t event, void *ResponseBuffer);
static void DisConnMqttServer_Callback(AT_ResponseEvent_t event, void *ResponseBuffer);
static void ReleaseClient_Callback(AT_ResponseEvent_t event, void *ResponseBuffer);
static void StopService_Callback(AT_ResponseEvent_t event, void *ResponseBuffer);
/* ==================================================================== */
/* ===================== All functions by section ===================== */
/* ==================================================================== */

/* Functions definitions go here, organised into sections */
/*********************************************************************************************************//**
 * @brief   : handle every 1s
 * @param
 * @retval  
 ************************************************************************************************************/
void MQTT_ManagerTask(void)
{
	if(Cellular_CheckReady2Use() == 1)
	{
		if(Mqtt_Manager.connect < TIME_GET_SIGNAL)
		{
			if(Cellular_CheckSignalInfo() == 0)
			{
				if(++Mqtt_Manager.connect >= TIME_GET_SIGNAL) 
				{
					DEBUG(".");
					Mqtt_Manager.connect = RETRY;
					Cellular_GetSignalLevel();
				}
			}
			else 
			{
				Mqtt_Manager.connect = CANCEL;
				Mqtt_Manager.needConnect = 1;
				Cellular_ClearSignalInfo();
			}
		}
	}
	
	if(Mqtt_Manager.needConnect == 1)
	{
		DEBUG("[MQTT] connecting\r\n");
		Mqtt_Manager.needConnect = 0;
		MQTT_Init();
	}
	
	if(Mqtt_Manager.timeoutConnection > 0)
	{
		if(--Mqtt_Manager.timeoutConnection == 0)
		{
			if(Cellular_GetBusyReason() == CELL_BUSY_MQTT)
				Cellular_SwitchState(CELL_IDLE);
			Mqtt_Manager.connect = CANCEL;
			DEBUG("[MQTT] Cancel connect\r\n");
		}
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void MQTT_Connect(void)
{
	if(Mqtt_Manager.connectStatus == MQTT_STT_DISCONNECTED)
	{
		Mqtt_Manager.timeoutConnection = DEFAULT_TIMEOUT_CONNECT;
		Mqtt_Manager.connect = RETRY;
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void MQTT_Retry(void)
{
	if(Mqtt_Manager.connectStatus == MQTT_STT_DISCONNECTED)
	{
		DEBUG("[MQTT] Retry connect to server\r\n");
		Mqtt_Manager.connect = RETRY;
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
void MQTT_Init(void)
{
	Cellular_Change2BusyState(CELL_BUSY_MQTT);
	
	Mqtt_Manager.connectStatus = MQTT_STT_DISCONNECTED;

	EN25Q_readData(FLASH_MQTT_AREA, &Mqtt_Params, sizeof(Mqtt_Params));
	if(Mqtt_Params.port == 0xFFFF)
	{
		memcpy(Mqtt_Params.acquireClient, DEFAULT_MQTT_CLIENT_NAME, sizeof(DEFAULT_MQTT_CLIENT_NAME));
		Mqtt_Params.clientIdx = DEFAULT_MQTT_CLI_IDX;
		memcpy(Mqtt_Params.serverAdd, DEFAULT_MQTT_IPADD, sizeof(DEFAULT_MQTT_IPADD));
		Mqtt_Params.port = DEFAULT_MQTT_PORT;
		Mqtt_Params.keepAliveTime = DEFAULT_MQTT_KEEP_TIME;
		Mqtt_Params.cleanSession = DEFAULT_MQTT_CLEAN_SESSION;
		Mqtt_Params.qos = DEFAULT_MQTT_QOS;
		memcpy(Mqtt_Params.username, DEFAULT_MQTT_USERNAME, sizeof(DEFAULT_MQTT_USERNAME));
		memcpy(Mqtt_Params.password, DEFAULT_MQTT_PASSWORD, sizeof(DEFAULT_MQTT_PASSWORD));
	}

	AT_SendATCommand(MQTT_START_SERVICE, "OK", "+CMQTTSTART:", 2000, 5, StartService_Callback);
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
uint8_t MQTT_PublishData(const char *topic, uint8_t qos, const char *input)
{
	if((Mqtt_Manager.connectStatus == MQTT_STT_CONNECTED) && (Mqtt_Manager.mesIsBeingSent == FALSE))
	{
		uint16_t topicLen;
		Mqtt_Manager.mesIsBeingSent = TRUE;
		
		Cellular_Change2BusyState(CELL_BUSY_MQTT);
		
		DEBUG("[MQTT] Pub to: %s, data: %s\r\n", topic, input);
		
		/* Input the topic of publish message */
		memset(bufTopic, 0, sizeof(bufTopic));
		topicLen = sprintf(bufTopic, "%s\r\n", topic) - 2;
		
		/* Input the publish message */
		memset(Mqtt_Params.TXBuf, 0, sizeof(Mqtt_Params.TXBuf));
		Mqtt_Params.TXlen = sprintf(Mqtt_Params.TXBuf, "%s", input);
		Mqtt_Params.qos = qos;
		
		memset(Mqtt_Params.bufContent, 0, sizeof(Mqtt_Params.bufContent));
		sprintf(Mqtt_Params.bufContent, MQTT_INPUT_PUB_TOPIC, Mqtt_Params.clientIdx, topicLen);
		AT_SendATCommand(Mqtt_Params.bufContent, ">", NULL, 1000, 5, InputTopic2Pub_Callback);
		return 1;
	}
	else if(Mqtt_Manager.mesIsBeingSent == TRUE)
	{
		DEBUG("[MQTT] Publish busy\r\n");
	}
	return 0;
}

/*********************************************************************************************************//**
 * @brief   +CMQTTCONNLOST: clientIndx,cause
 * @paramtf
 * @retval  
 ************************************************************************************************************/
void MQTT_Client_Disconnect_Passively(void *ResponseBuffer)
{
	char *tok;
	uint8_t cindx;
	uint8_t cause;
	tok = strtok((char*)ResponseBuffer, ":");
	tok = strtok(NULL, ",");
	cindx = atoi(tok);
	tok = strtok(NULL, "\r\n");
	cause = atoi(tok);
	
	Mqtt_Manager.connectStatus = MQTT_STT_DISCONNECTED;
	GPIO_WriteOutBits(CELL_LED_PORT, CELL_LED_PIN, LED_OFF);
	DEBUG("[MQTT] Client Disconnect Passively, index: %d ", cindx);
	switch(cause)
	{
		case 1: DEBUG("Socket is closed passively"); break;
		case 2: DEBUG("Socket is reset"); break;
		case 3: DEBUG("Network is closed"); break;
	}
	DEBUG("\r\n");
	
	MQTT_Connect();
}
/* ==================================================================== */
/* ====================== private functions =========================== */
/* ==================================================================== */

/*********************************************************************************************************//**
 * @brief   : 
 * @param		: eg: buf +CMQTTSTART:0\r\n, 		 rest +CMQTTSTART, errPos 0
								  buf +CMQTTCONNECT:0,0\r\n, rest +CMQTTSTART, errPos 1
 * @retval  
 ************************************************************************************************************/
static uint8_t checkCRC(const char *buf, const char *rest, uint8_t errPos)
{
	char *tok, *tem;
	uint8_t err;
	tem = strstr(buf, rest);
	tok = strtok(tem, ":");
	if(errPos == 0) tok = strtok(NULL, "\r\n");
	else if(errPos == 1)
	{
		tok = strtok(NULL, ",");
		tok = strtok(NULL, "\r\n");
	}
		
	err = atoi(tok);
	DEBUG("[MQTT] Check CRC: ");
	switch(err)
	{
		case 0:  DEBUG("operation succeeded"); break;
		case 1:  DEBUG("failed"); break;
		case 2:  DEBUG("bad UTF-8 string"); break;
		case 3:  DEBUG("sock connect fail"); break;
		case 4:  DEBUG("sock create fail"); break;
		case 5:  DEBUG("sock close fail"); break;
		case 6:  DEBUG("message receive fail"); break;
		case 7:  DEBUG("network open fail"); break;
		case 8:  DEBUG("network close fail"); break;
		case 9:  DEBUG("network not opened"); break;
		case 10: DEBUG("client index error"); break;
		case 11: DEBUG("no connection"); break;
		case 12: DEBUG("invalid parameter"); break;
		case 13: DEBUG("not supported operation"); break;
		case 14: DEBUG("client is busy"); break;
		case 15: DEBUG("equire connection fail"); break;
		case 16: DEBUG("sock sending fail"); break;
		case 17: DEBUG("timeout"); break;
		case 18: DEBUG("topic is empty"); break;
		case 19: DEBUG("client is used"); break;
		case 20: DEBUG("client not acquired"); break;
		case 21: DEBUG("client not released"); break;
		case 22: DEBUG("length out of range"); break;
		case 23: DEBUG("network is opened"); break;
		case 24: DEBUG("packet fail"); break;
		case 25: DEBUG("DNS error"); break;
		case 26: DEBUG("socket is closed by server"); break;
		case 27: DEBUG("connection refused: unaccepted protocol version"); break;
		case 28: DEBUG("connection refused: identifier rejected"); break;
		case 29: DEBUG("connection refused: server unavailable"); break;
		case 30: DEBUG("connection refused: bad user name or password"); break;
		case 31: DEBUG("connection refused: not authorized"); break;
		case 32: DEBUG("handshake fail"); break;
		case 33: DEBUG("not set certificate"); break;
		case 34: DEBUG("Open session failed"); break;
		case 35: DEBUG("Disconnect from server failed"); break;
	}
	DEBUG("\r\n");
	return err;
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void StartService_Callback(AT_ResponseEvent_t event, void *ResponseBuffer)
{
	uint8_t err;
	if(event == EVENT_OK)
	{
		err = checkCRC((const char*)ResponseBuffer, "+CMQTTSTART:", 0);
		if(err == 0) 
		{			
			memset(Mqtt_Params.bufContent, 0, sizeof(Mqtt_Params.bufContent));
			sprintf(Mqtt_Params.bufContent, MQTT_ACQUIRE_CLIENT, Mqtt_Params.clientIdx, Mqtt_Params.acquireClient);
			AT_SendATCommand(Mqtt_Params.bufContent, "OK", NULL, 2000, 5, AcquireClient_Callback);
		}
		// Todo: else
	}
	else if(event == EVENT_ERROR)
	{
		err = checkCRC((const char*)ResponseBuffer, "+CMQTTSTOP:", 0);
		if(err == 19) 
		{
			Mqtt_Manager.step = 0;
			AT_SendATCommand("AT+CMQTTDISC?\r\n", "+CMQTTDISC:", "+CMQTTDISC:", 2000, 5, DisConnMqttServer_Callback);
		}
	}
	else if(event == EVENT_TIMEOUT)
	{
		MQTT_Retry();
	}
}
 
/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void AcquireClient_Callback(AT_ResponseEvent_t event, void *ResponseBuffer)
{
	if(event == EVENT_OK)
	{		
		memset(Mqtt_Params.bufContent, 0, sizeof(Mqtt_Params.bufContent));
		sprintf(Mqtt_Params.bufContent, MQTT_CONNECT_SERVER, Mqtt_Params.clientIdx, Mqtt_Params.serverAdd, Mqtt_Params.port, Mqtt_Params.keepAliveTime, Mqtt_Params.cleanSession);		
		AT_SendATCommand(Mqtt_Params.bufContent, "OK", "+CMQTTCONNECT:", 20000, 3, Conn2MqttServer_Callback);
	}
	else
	{
		MQTT_Retry();
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void Conn2MqttServer_Callback(AT_ResponseEvent_t event, void *ResponseBuffer)
{
	uint8_t err;
	if(event == EVENT_OK)
	{
		err = checkCRC((const char*)ResponseBuffer, "+CMQTTCONNECT:", 1);
		if(err == 0) 
		{			
			Mqtt_Manager.step = 0;
			Mqtt_Manager.connectStatus = MQTT_STT_CONNECTED;
			DEBUG("MQTT] Connected\r\n");
			Mqtt_Manager.timeoutConnection = 0;
			InputTopic2Sub_Callback(EVENT_OK, NULL);
		}
		else
		{
			MQTT_Retry();
		}
	}
	else
	{
		MQTT_Retry();
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void InputTopic2Sub_Callback(AT_ResponseEvent_t event, void *ResponseBuffer)
{
	uint8_t topicLen;
	if(event == EVENT_OK)
	{
		switch(Mqtt_Manager.step)
		{
			case 0:
				topicLen = strlen(TOPIC_CONTROL);
				memset(Mqtt_Params.bufContent, 0, sizeof(Mqtt_Params.bufContent));
				sprintf(Mqtt_Params.bufContent, MQTT_INPUT_SUB_TOPIC, Mqtt_Params.clientIdx, topicLen, Mqtt_Params.qos);
				AT_SendATCommand(Mqtt_Params.bufContent, ">", NULL, 1000, 5, InputTopic2Sub_Callback);
				break;
			case 1:
				AT_SendATCommand(TOPIC_CONTROL, "OK", NULL, 1000, 5, InputTopic2Sub_Callback);
				break;		
			case 2:
				topicLen = strlen(TOPIC_UPDATE);
				memset(Mqtt_Params.bufContent, 0, sizeof(Mqtt_Params.bufContent));
				sprintf(Mqtt_Params.bufContent, MQTT_INPUT_SUB_TOPIC, Mqtt_Params.clientIdx, topicLen, Mqtt_Params.qos);
				AT_SendATCommand(Mqtt_Params.bufContent, ">", NULL, 1000, 5, InputTopic2Sub_Callback);
				break;
			case 3:
				AT_SendATCommand(TOPIC_UPDATE, "OK", NULL, 1000, 5, InputTopic2Sub_Callback);
				break;	
			case 4:			
				memset(Mqtt_Params.bufContent, 0, sizeof(Mqtt_Params.bufContent));
				sprintf(Mqtt_Params.bufContent, MQTT_SUB_MES, Mqtt_Params.clientIdx);
				AT_SendATCommand(Mqtt_Params.bufContent, "OK", "+CMQTTSUB:", 20000, 3, Sub_Callback);
				break;
		}
		Mqtt_Manager.step++;
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void Sub_Callback(AT_ResponseEvent_t event, void *ResponseBuffer)
{
	uint8_t err;
	if(event == EVENT_OK)
	{
		err = checkCRC((const char*)ResponseBuffer, "+CMQTTSUB:", 1);
		if(err == 0) 
		{			
			DEBUG("[MQTT] Sub done, ready to use\r\n");
			MQTT_PublishData(TOPIC_EVENT, 2, "MODEM_CONNECTED");
			GPIO_WriteOutBits(CELL_LED_PORT, CELL_LED_PIN, LED_ON);
			Cellular_SwitchState(CELL_IDLE);
		}
		else
		{
			MQTT_Retry();
		}
	}
	else
	{
		MQTT_Retry();
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void DisConnMqttServer_Callback(AT_ResponseEvent_t event, void *ResponseBuffer)
{
	uint8_t stt;
	char tem[14] = {0};
	char *tok, *buf;
	
	if(event == EVENT_OK)
	{
		switch(Mqtt_Manager.step)
		{
			case 0:
				sprintf(tem, "+CMQTTDISC: %d", Mqtt_Params.clientIdx);
				DEBUG("tem: %s\r\n", tem);
				// +CMQTTDISC: 0,0
				buf = strstr((const char*)ResponseBuffer, tem); // +CMQTTDISC: <index>
				tok = strtok(buf, ",");
				tok = strtok(NULL, "\r\n");
				stt = atoi(tok);
				if(stt == 0) // connection
				{
					memset(Mqtt_Params.bufContent, 0, sizeof(Mqtt_Params.bufContent));
					sprintf(Mqtt_Params.bufContent, MQTT_DISCONN_SERVER, Mqtt_Params.clientIdx);
					AT_SendATCommand(Mqtt_Params.bufContent, "OK", "+CMQTTDISC:", 20000, 3, DisConnMqttServer_Callback);
				}
				else
				{
					memset(Mqtt_Params.bufContent, 0, sizeof(Mqtt_Params.bufContent));
					sprintf(Mqtt_Params.bufContent, MQTT_RELEASE_CLIENT, Mqtt_Params.clientIdx);
					AT_SendATCommand(Mqtt_Params.bufContent, "OK", NULL, 1000, 5, ReleaseClient_Callback);
				}
				break;
			case 1:
				stt = checkCRC((const char*)ResponseBuffer, "+CMQTTDISC:", 1);
				if(stt == 0)
				{
					memset(Mqtt_Params.bufContent, 0, sizeof(Mqtt_Params.bufContent));
					sprintf(Mqtt_Params.bufContent, MQTT_RELEASE_CLIENT, Mqtt_Params.clientIdx);
					AT_SendATCommand(Mqtt_Params.bufContent, "OK", NULL, 1000, 5, ReleaseClient_Callback);
				}
				break;
		}
		Mqtt_Manager.step++;
	}
}
/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void ReleaseClient_Callback(AT_ResponseEvent_t event, void *ResponseBuffer)
{
	uint8_t err;
	if(event == EVENT_OK)
	{
		AT_SendATCommand(MQTT_STOP_SERVICE, "OK", "+CMQTTSTOP:", 1000, 5, StopService_Callback);
	}
	else if(event == EVENT_ERROR)
	{
		err = checkCRC((const char*)ResponseBuffer, "+CMQTTREL:", 1);
		if(err == 19) 
		{
			// todo: ?
		}
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void StopService_Callback(AT_ResponseEvent_t event, void *ResponseBuffer)
{
	uint8_t err;
	if(event == EVENT_OK)
	{
		err = checkCRC((const char*)ResponseBuffer, "+CMQTTSTOP:", 0);
		if(err == 0) 
		{			
			DEBUG("[MQTT] Stop service ok\r\n");
			GPIO_WriteOutBits(CELL_LED_PORT, CELL_LED_PIN, LED_OFF);
			Mqtt_Manager.connectStatus = MQTT_STT_DISCONNECTED;
			Cellular_SwitchState(CELL_IDLE);
			
			MQTT_Retry();
		}
		// Todo: else
	}
}
/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void InputTopic2Pub_Callback(AT_ResponseEvent_t event, void *ResponseBuffer)
{
	if(event == EVENT_OK)
	{
		Mqtt_Manager.step = 0;
		AT_SendATCommand(bufTopic, "OK", NULL, 1000, 5, InputMes2Pub_Callback);
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void InputMes2Pub_Callback(AT_ResponseEvent_t event, void *ResponseBuffer)
{
	if(event == EVENT_OK)
	{
		switch(Mqtt_Manager.step)
		{
			case 0:
				memset(Mqtt_Params.bufContent, 0, sizeof(Mqtt_Params.bufContent));
				sprintf(Mqtt_Params.bufContent, MQTT_INPUT_PUB_MES, Mqtt_Params.clientIdx, Mqtt_Params.TXlen);				
				AT_SendATCommand(Mqtt_Params.bufContent, ">", NULL, 1000, 5, InputMes2Pub_Callback);
				break;
			case 1:
				AT_SendATCommand(Mqtt_Params.TXBuf, "OK", NULL, 1000, 5, InputMes2Pub_Callback);
				break;
			case 2:
				memset(Mqtt_Params.bufContent, 0, sizeof(Mqtt_Params.bufContent));
				sprintf(Mqtt_Params.bufContent, MQTT_PUB_MES, Mqtt_Params.clientIdx, Mqtt_Params.qos);				
				AT_SendATCommand(Mqtt_Params.bufContent, "OK", "+CMQTTPUB:", 20000, 3, Pub_Callback);
				break;
		}
		Mqtt_Manager.step++;
	}
}

/*********************************************************************************************************//**
 * @brief   
 * @param
 * @retval  
 ************************************************************************************************************/
static void Pub_Callback(AT_ResponseEvent_t event, void *ResponseBuffer)
{
	uint8_t err;
	
	if(event == EVENT_OK)
	{
		err = checkCRC((const char*)ResponseBuffer, "+CMQTTPUB:", 1);
		if(err == 0) 
		{			
			DEBUG("[MQTT] Pub done\r\n");
			Mqtt_Manager.mesIsBeingSent = FALSE;
		}
		// Todo: else
	}
	else if(event == EVENT_ERROR)
	{
		DEBUG("[MQTT] Pub err: %s\r\n", (char*)ResponseBuffer);
	}
	else if(event == EVENT_TIMEOUT)
	{
		DEBUG("[MQTT] Pub timeout: %s\r\n", (char*)ResponseBuffer);
	}
	
	Cellular_SwitchState(CELL_IDLE);
}
