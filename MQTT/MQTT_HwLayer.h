/*
 * MQTT_HwLayer.h
 *
 *  Created on: Nov 01, 2020
 *      Author: chungnt@epi-tech.com.vn
 *
 *      
*/
#ifdef __cplusplus
extern "C"
{
#endif

#ifndef _MQTT_HWLAYER_H_
#define _MQTT_HWLAYER_H_


/* ==================================================================== */
/* ========================== include files =========================== */
/* ==================================================================== */

/* Inclusion of system and local header files goes here */
#include "All_User_Lib.h"

/* ==================================================================== */
/* ============================ constants ============================= */
/* ==================================================================== */

/* #define and enum statements go here */
#define TOPIC_DATA				"chungnt/data"		// Pub
#define TOPIC_EVENT				"chungnt/event"		// Pub
#define TOPIC_CONTROL			"chungnt/control" // Sub
#define TOPIC_UPDATE			"chungnt/update"	// Sub

#define DEFAULT_MQTT_CLIENT_NAME		"chungnt"
#define DEFAULT_MQTT_CLI_IDX				0
#define DEFAULT_MQTT_IPADD					"broker.hivemq.com"
#define DEFAULT_MQTT_PORT						1883
#define DEFAULT_MQTT_KEEP_TIME			60
#define DEFAULT_MQTT_CLEAN_SESSION	1
#define DEFAULT_MQTT_QOS						2
#define DEFAULT_MQTT_USERNAME				"chungnt"
#define DEFAULT_MQTT_PASSWORD				"123456a@"

typedef enum
{
	MQTT_STT_DISCONNECTED = 0,
	MQTT_STT_CONNECTED,
} MQTT_CONNECT_STT_E;

typedef struct
{
	uint8_t connect				: 3;
	uint8_t needConnect 	: 1;
	uint8_t connectStatus : 1;
	uint8_t step					: 3;
	
	uint8_t mesIsBeingSent;
	uint16_t timeoutConnection;	
	uint16_t timeoutPublish;
} Mqtt_Manager_t;

typedef struct
{
	char RXBuf[1024];
	uint16_t RXlen;
	char TXBuf[1024];
	uint16_t TXlen;
	char bufContent[50];
	/*======================*/
	uint8_t acquireClient[20];
	uint8_t clientIdx;
	char serverAdd[30];
	uint16_t port; 
	uint8_t keepAliveTime;
	uint8_t cleanSession;
	uint8_t qos;
	uint8_t username[20]; // max 256bytes
	uint8_t password[20]; // max 256bytes
} Mqtt_Params_t;
/* ==================================================================== */
/* ========================== public data ============================= */
/* ==================================================================== */

/* Definition of public (external) data types go here */


/* ==================================================================== */
/* ======================= public functions =========================== */
/* ==================================================================== */

/* Function prototypes for public (external) functions go here */
void MQTT_ManagerTask(void);
void MQTT_Init(void);
void MQTT_Connect(void);
void MQTT_Retry(void);
uint8_t MQTT_PublishData(const char *topic, uint8_t qos, const char *input);
/* URC */
void MQTT_Client_Disconnect_Passively(void *ResponseBuffer);
#endif
#ifdef __cplusplus
}
#endif
